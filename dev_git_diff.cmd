@echo off
rem marvil07:
rem git status --branch --short | sed -e 's/.*\.\.\.//' -e 's/ .*//'
rem git config --get-regexp branch.`git branch | grep ^* | sed 's/..//'`.*

rem msonnabaum:
rem git remote show origin -n | grep $(git branch | grep '*'| awk '{print $2}') | awk '{print $5}'

rem For /F "Tokens=2" %%I in ('Date /T') Do Set StrDate=%%I
rem For /F "Tokens=*" %%I in ('Time /T') Do Set StrTime=%%I
rem For /F "Tokens=2" %%I in ('<any command>') Do Set <variable name>=%%I
rem For /F "Skip=3 Tokens=*" %%I in ('ping %StrServer% -n 1') Do Set StrAnswer=%%I

rem for /F "tokens=2" %%I in ('git status --branch --short|sed -e""') do echo %%I


rem git status --branch --short|sed -e"/^[^#]/d; s/^.*\.\.\.([^ ]*).*/$1/"
rem @echo on
rem git status --branch --short|sed -n "s/.*\.\.\.\([^ ]*\).*/\1/p;"

rem for /F "usebackq" %%I in (`git status --branch --short|sed -n "s/.*\.\.\.\([^ ]*\).*/\1/p;"`) do echo %%I
cd /D "%~f1"
for /F %%I in ('git status --branch --short^|sed -n "s/.*\.\.\.\([^ ]*\).*/\1/p;"') do set branch=%%I
git diff %branch% > "%~nx1.patch"

rem bash -c 'git status --branch --short | sed -n "s/.*\.\.\.\([^ ]*\).*/\1/p;" | git diff --no-prefix '
rem git diff $(git status --branch --short | sed -n "s/.*\.\.\.\([^ ]*\).*/\1/p;")
